import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SiderbarComponent } from './components/siderbar/siderbar.component';
import { PrincesComponent } from './components/princes/princes.component';
import { AboutComponent } from './components/about/about.component';
import { Hija1Component } from './components/princes/hija1.component';
import { Hija2Component } from './components/princes/hija2.component';
import { Hija3Component } from './components/princes/hija3.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SiderbarComponent,
    PrincesComponent,
    AboutComponent,
    Hija1Component,
    Hija2Component,
    Hija3Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
