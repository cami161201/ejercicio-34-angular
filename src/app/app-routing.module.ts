import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { PrincesComponent } from './components/princes/princes.component';
import { RUTAS } from './components/princes/princes.component';
const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'prices/:id',
    component: PrincesComponent,
    children: RUTAS
  },
  { path:'about', component:AboutComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
