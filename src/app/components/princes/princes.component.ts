import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import { Hija1Component } from './hija1.component';
import { Hija2Component } from './hija2.component';
import { Hija3Component } from './hija3.component';
import { ActivatedRoute } from '@angular/router';


export const RUTAS: Routes = [
  { path: '20', component: Hija1Component },
  { path: '50', component: Hija2Component },
  { path: '80', component: Hija3Component },
  { path: '**', pathMatch: 'full', redirectTo: '20' },
];


@Component({
  selector: 'app-princes',
  templateUrl: './princes.component.html',
  styleUrls: ['./princes.component.css'],
})
export class PrincesComponent implements OnInit {
  constructor(private router:ActivatedRoute) {
    router.params.subscribe(parametros =>{
      console.log('ruta hija - 20');
      console.log(parametros);
      
      
    });
    router.parent?.params.subscribe(parametros =>{
      console.log('ruta padre');
      
    })
  }

  ngOnInit(): void {}
}
