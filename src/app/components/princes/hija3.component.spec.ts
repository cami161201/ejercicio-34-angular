import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hija3Component } from './hija3.component';

describe('Hija3Component', () => {
  let component: Hija3Component;
  let fixture: ComponentFixture<Hija3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hija3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hija3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
